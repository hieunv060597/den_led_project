(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
			stuck_header();
			drop_content_mb();
			banner_homepage();
			slider_client();
			galerry_product();
			widget_right_slide();
			widget_blog_slide();
			back_to_top();
        });              
    };
})(jQuery);

function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function stuck_header(){
	var header_main = document.querySelector('#header .header-main');
	var header_bottom = document.querySelector('#header .header-bottom');
	var check = true;
	
	window.addEventListener('scroll', function(){
		if(window.pageYOffset > 260){
			if(check == true){
				header_main.classList.add("none-scroll");
				header_bottom.classList.add("stuck-header");
				check = false;
			}
		}
		else{
			if(check == false){
				header_main.classList.remove("none-scroll");
				header_bottom.classList.remove("stuck-header");
				check = true;
			}
		}
	})
}



function drop_content_mb(){
	var has_drop_menu = $('#wrapper .moblie-nav .mb-ul-nav .has-child');
	var drop_list_mb = $('#wrapper .moblie-nav .mb-ul-nav .has-child .drop-down');
	if(has_drop_menu.length == 0){
		return 0;
	}
	else{
		has_drop_menu.append("<button class = 'btn-drop-nav-menu'><i class='fa fa-caret-right'></i></button>");
	}
	var btn_click_drop = $('.btn-drop-nav-menu');
	if(btn_click_drop.length == 0){
		return 0;
	}
	else{
		for(var i = 0; i< btn_click_drop.length; i++){
			btn_click_drop[i].onclick = function(){
				this.classList.toggle("active");	
				for(var k = 0; k< drop_list_mb.length; k++){
					var panel = drop_list_mb[k].nextElementSibling;
					if(panel.classList[1] == "active"){
						drop_list_mb[k].classList.add("show-drop-down");
					}
					else{
						drop_list_mb[k].classList.remove("show-drop-down");
					}
				}
			}
		} 
	}
}

function banner_homepage(){
	$('#home-page .banner .owl-carousel').owlCarousel({
		loop:true,
		margin:0,
		nav:true,
		dots: true,
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		 0:{
			items:1,
		 },
		 576:{
		   items:1,
		 },
		 768:{
		   items:1,
		},
		1024:{
		   items:1,
		},

		1400:{
		   items:1,
		}
	 }
   })
}

function slider_client(){
	$('#home-page .client-feed-back .slider-feed-back .owl-carousel').owlCarousel({
		loop:true,
		margin:0,
		nav:false,
		dots: true,
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		 0:{
			items:1,
		 },
		 576:{
		   items:1,
		 },
		 768:{
		   items:1,
		},
		1024:{
		   items:1,
		},

		1400:{
		   items:1,
		}
	 }
   })
}

function galerry_product(){
	$('#product-detail .infor-product .product-galerry .slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		arrows: true,
		fade: true,
		asNavFor: '.slider-nav',
		autoplay: true,
        autoplaySpeed: 5000,
	});

	$('#product-detail .infor-product .product-galerry .slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		autoplay: true,
        autoplaySpeed: 5000,
		dots: false,
		centerMode: true,
		centerPadding: '2px',
		arrows: true,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 1800,
				settings: {
				  slidesToShow: 5,
				}
			  },
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 5,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 4,
			  }
			},
			{
				breakpoint: 576,
				settings: {
				  slidesToShow: 3,
				}
			  },
			{
			  breakpoint: 320,
			  settings: {
				slidesToShow: 2,
			  }
			}
		]
	});
}

function widget_right_slide(){
	$('#product-detail .product-tabs .right-widget-product .owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		dots: false,
		nav:true,
		navText: ["<i class='fa fa-caret-left'></i>","<i class='fa fa-caret-right'></i>"],
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},

		576:{
		   items:1,
		},

		768:{
		   items:1,
		},
		1024:{
			items:1,		   
		},

		1400:{
		   items:1,
		}
	}
  })
}

function widget_blog_slide(){
	$('#blog-detail .main-blog-detail .right-widget-product .owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		dots: false,
		nav:true,
		navText: ["<i class='fa fa-caret-left'></i>","<i class='fa fa-caret-right'></i>"],
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},

		576:{
		   items:1,
		},

		768:{
		   items:1,
		},
		1024:{
			items:1,		   
		},

		1400:{
		   items:1,
		}
	}
  })
}

(function($) {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
})(jQuery);